# ALSA USB Hotplug #

### Summary ###

A simple udev rule for hotplugging a USB sound card. When the USB card (card no. 2) is plugged in it is set as the ALSA default, and when it is unplugged the internal card (no. 0) becomes the default again. Dmix is used to prevent a single program from taking over the whole device.

### Usage ###

Copy 10-alsa.rules under /etc/udev/rules.d/ and asound.usb under /etc/asound.d/. If you wish to identify the card by its name instead of its number you can find it with the command `aplay -l | awk -F \: '/,/{print $2}' | awk '{print $1}' | uniq`.

WARNING: My internal sound card works as I like it without anything in asound.conf, so I have written the udev rule to simply remove the file when the USB interface is unplugged. If there's something you need to have in asound.conf, you can put it in asound.internal, for example, and replace the second line with `KERNEL=="pcmC[D0-9cp]*", ACTION=="add", PROGRAM="/bin/sh -c 'ln -sf /etc/asound.d/asound.internal /etc/asound.conf;'"`.
